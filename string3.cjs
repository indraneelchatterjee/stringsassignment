let string3 = (string) => {
  const ipv4Regex = /^(\d{1,3}\.){3}\d{1,3}$/;
  if (!ipv4Regex.test(string)) {
    return [];
  }
  let arr = string.split(".");
  let ans = [];
  arr.forEach((elem) => {
    ans.push(Number(elem));
  });
  return ans;
};

module.exports = string3;
