let string1 = (string) => {
  let ans = string.replace(/[$,]/g, "");
  if (isNaN(ans)) {
    return 0;
  }
  return ans;
};

module.exports = string1;
