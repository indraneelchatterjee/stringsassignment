let string4 = (obj) => {
  let ans = "";
  for (const key in obj) {
    let lower = obj[key].toLowerCase();
    lower = lower.charAt(0).toUpperCase() + lower.slice(1);
    ans += lower + " ";
  }
  return ans;
};

module.exports = string4;
