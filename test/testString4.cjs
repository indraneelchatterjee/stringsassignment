const string4 = require("../string4.cjs");

console.log(string4({ first_name: "JoHN", last_name: "SMith" }));
console.log(
  string4({ first_name: "JoHN", middle_name: "doe", last_name: "SMith" })
);
