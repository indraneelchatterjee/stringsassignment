let string5 = (arr) => {
  if (arr.length === 0) {
    return "";
  }
  let ans = "";
  for (let index = 0; index < arr.length; index++) {
    ans += arr[index] + " ";
  }
  return ans;
};

module.exports = string5;
