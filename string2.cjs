let string2 = (string) => {
  let arr = string.split(".");
  let ans = [];
  arr.forEach((elem) => {
    ans.push(Number(elem));
  });
  return ans;
};

module.exports = string2;
